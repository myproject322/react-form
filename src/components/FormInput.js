import React from "react";
import "./formInput.css";

export default function FormInput(props) {
  const { label, onChange, id, errorMessage, ...inputProps } = props;
  return (
    <div className="formInput">
      <label htmlFor="">{label}</label>
      <input {...inputProps} onChange={onChange} />
      <span className="errorMes">{errorMessage}</span>
    </div>
  );
}
