import { useRef, useState } from "react";
import "./App.css";
import FormInput from "./components/FormInput";

function App() {
  const [values, setValues] = useState({
    username: "",
    password: "",
    email: "",
    confirmpassword: "",
  });
  const inputs = [
    {
      id: 1,
      name: "username",
      placeholder: "User Name",
      label: "Username",
      type: "text",
      errorMessage:
        "Username should be 3-16 characters and shouldn't include any special character",
      required: true,
      pattern: "^[a-zA-Z0-9]{3,16}$",
    },
    {
      id: 2,
      name: "email",
      placeholder: "Email",
      label: "Email",
      type: "email",
      errorMessage: "It should be a valid email address!",
      required: true,
    },
    {
      id: 3,
      name: "birthday",
      placeholder: "Birthday",
      label: "Birthday",
      type: "date",
      errorMessage: "Please input!",
    },
    {
      id: 4,
      name: "password",
      placeholder: "Password",
      label: "Password",
      type: "password",
      errorMessage:
        "Password should be 8-20 characters and include at least 1 letter, 1 number",
      pattern: "^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$",
      required: true,
    },
    {
      id: 5,
      name: "confirmPassword",
      placeholder: "Confirm Password",
      label: "Confirm Password",
      type: "password",
      errorMessage: "Password don't match!",
      pattern: values.password,
      required: true,
    },
  ];

  console.log(values);

  const onChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };
  return (
    <div className="App">
      <form>
        {inputs.map((item) => {
          return (
            <FormInput
              key={item.id}
              {...item}
              value={values[item.name]}
              onChange={onChange}
            />
          );
        })}
      </form>
    </div>
  );
}

export default App;
